<?php 
include __DIR__.'/../xyz/menu.php';

if(isset($_get['id'])){
    $MEMBER_ = $_get['id'];
}
$MSG_ = '';
if(isset($_get['msg'])){
    $MSG_ = $_get['msg'];
}


$tmp_ref = "";
if(isset($request[3])){
    $tmp_ref = "../";
}

$EVENT = $_SESSION['user']['Event'];

$SEETLYTOEVA = "N/A";
$JUMLAH = "N/A";


$pageNumber = 1;
$pageSize = 25;


if(isset($_get['pageNumber'])){
    $pageNumber = $_get['pageNumber'];
}

if(isset($_get['pageSize'])){
    $pageSize = $_get['pageSize'];
}

if(isset($_get['search'])){
    $_get['search'] = urldecode($_get['search']);
    $cari = $_get['search'];
    $search = '&search='.$_get['search'];
}else{
    $search = "";
}

$ch = curl_init();               
$url_ = $titu."api/v1/resources/event_jersey/".$request[3];
// set url
curl_setopt($ch, CURLOPT_URL, $url_);
// return the transfer as a string 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $output contains the output string 
$output = curl_exec($ch); 
// tutup curl 
curl_close($ch); 
$crud = json_decode($output);







?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Steelytoe Xyz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>Xyz</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Steelytoe</b>Xyz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> <?php echo $_SESSION['user']['EventName']; ?> &nbsp; </span>
              <i class="fa fa-calendar"> </i>
              <span class="label label-success"><?php echo COUNT($_SESSION['user']['Events']); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo COUNT($_SESSION['user']['Events']); ?> events</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php
                    foreach($_SESSION['user']['Events'] AS $vall){
                        echo "<li><a href='".'../xyz/event/'.$vall->evnhId."'><h3>".$vall->evnhName."</i></h3></a></li>";
                    }
                  
                ?>
 
                </ul>
              </li>
              <li class="footer"><a href="#">Close</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out"> </i>
            </a>

              
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    

    $menu = str_replace("{{menu_quota}}","class= menu-open",$menu);
    $menu = str_replace("{{menu_quota_show}}","display: block;",$menu);
	$menu = str_replace("{{quota_jersey}}","active menu-open",$menu);
    echo $menu;
  
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Quota Jersey
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-tachometer-alt"></i> Home</a></li>
        <li class="active">Participant Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
	  
	  
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
			  <li class="active"><a href="#timeline" data-toggle="tab">Jersey <?php echo $crud->linked->evjsMshiId[0]->mshiDesc; ?></a></li>  
            </ul>
            <div class="tab-content">  
              <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">

              <form  action="../update_quota_jersey.php" method="get"  class="form-horizontal">
                  <div class="form-group">
                    <label disabled for="inputName" class="col-sm-2 control-label">Change Quota Jersey <?php echo $crud->linked->evjsMshiId[0]->mshiDesc; ?>: </label>
   
                    <div class="col-sm-7">
                      <input  name="quota_total" type="number" class="form-control" id="inputName" placeholder="<?php echo $crud->data->evjsQuota; ?>" required>
                      <input  name="quota_id" type="hidden" class="form-control" value ="<?php echo $crud->data->id  ?>" required>
					</div>
                
                    <div class="col-sm-1">
                      <button  type="submit" class="btn btn-danger">Update</button>
                    </div>
					
                  </div>
              </form>
              
              
                
              </div>
              
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2019 <a href="#">SteelytoeXyz</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $tmp_ref; ?>../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $tmp_ref; ?>../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $tmp_ref; ?>../dist/js/demo.js"></script>


  
  
</script>

</body>
</html>
