<?php 
include __DIR__.'/../xyz/menu.php';


$tmp_ref = "";
if(isset($request[3])){
    $tmp_ref = "../";
}


$EVENT = $_SESSION['user']['Event'];

$SEETLYTOEVA = "N/A";
$JUMLAH = "N/A";


$pageNumber = 1;
$pageSize = 10;


if(isset($_get['pageNumber'])){
    $pageNumber = $_get['pageNumber'];
}

if(isset($_get['pageSize'])){
    $pageSize = $_get['pageSize'];
}

if(isset($_get['search'])){
    $_get['search'] = urldecode($_get['search']);
    $cari = $_get['search'];
    $search = '&search='.$_get['search'];
}else{
    $search = "";
}




?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Steelytoe Xyz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>Xyz</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Steelytoe</b>Xyz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> <?php echo $_SESSION['user']['EventName']; ?> &nbsp; </span>
              <i class="fa fa-calendar"> </i>
              <span class="label label-success"><?php echo COUNT($_SESSION['user']['Events']); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo COUNT($_SESSION['user']['Events']); ?> events</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php
                    foreach($_SESSION['user']['Events'] AS $vall){
                        echo "<li><a href='".'../xyz/event/'.$vall->evnhId."'><h3>".$vall->evnhName."</i></h3></a></li>";
                    }
                  
                ?>
 
                </ul>
              </li>
              <li class="footer"><a href="#">Close</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out"> </i>
            </a>

              
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    

    $menu = str_replace("{{refundtransaction}}","class='active'",$menu);
    echo $menu;
  
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Refund Transaction
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Refund Transaction</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- left column -->
        <div class="col-md-12 table-responsive">
          

          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">DATA REFUND TRANSACTION</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-2">
                       
                    </div>
                    
               
                    
                        <!--
                        <div class="dataTables_length" id="example2_length">
                            <label>Show <select name="example2_length" aria-controls="example2" class="form-control input-sm" disabled>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entries</label>
                             
                        </div>
                        
                        -->
               
            
                    <div class="col-sm-10">
                        <div id="example2_filter" class="dataTables_filter">
                            <form>

                                <label>Search By Name : <input value ="<?php if(isset($_get['search'])) echo $_get['search']; ?>" name="search" type="search" class="form-control input-sm" placeholder="" aria-controls="example2"></label>
                            </form>
                        </div>
                    </div>
                </div>
               
                
             


			
             <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
				<?php if($EVENT==150) echo "<th>ID</th>"; ?>
                  <th>Date & Time</th>
                  <th>Order ID</th>
                  
                  <th>Customer Name</th>
                  <th>Email</th>
                  <th>Payment Type</th>
                  <th>Amount</th>

                  <th>Status</th>
                </tr>
                

                </thead>
                <tbody>
                
                <?php
                    

                    // GET DATA
                    $ch = curl_init(); 
                    
                    
                    if(isset($cari)){
                        $url_ = $titu."api/v1/resources/refund?pageNumber=$pageNumber&pageSize=$pageSize&filter[refuEventId]=$EVENT&filter[refuUserName][like]=%25".urlencode($cari)."%25";
                    }else{
                        $url_ = $titu."api/v1/resources/refund?pageNumber=$pageNumber&pageSize=$pageSize&filter[refuEventId]=$EVENT";
                    }
                    
              

                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url_);

                    // return the transfer as a string 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                

                    // $output contains the output string 
                    $output = curl_exec($ch); 

                    // tutup curl 
                    curl_close($ch);      

                    // menampilkan hasil curl
                    $data_all = json_decode($output);
                    
                    
                    if(isset($data_all->linked->refuTrnsId )){
						foreach($data_all->linked->refuTrnsId  as $v){
							$data_all->linked->refuTrnsId [$v->id] = $v;
						}
					}
                    
                    if(isset($data_all->linked->trnsPaymentType)){
						foreach($data_all->linked->trnsPaymentType as $v){
							$data_all->linked->trnsPaymentType[$v->id] = $v;
						}
					}
                    
  

                    
                    if(isset($data_all->data)){
                        
                        foreach($data_all->data as $vall ){
                            echo "<tr>";
							
							if($EVENT==150) echo "<td>".$vall->trnsId."</td>";
                            
                             echo "<td>".$vall->refuRequestTime."</a></td>";
                           // echo "<td> <a href='transaction/".$vall->trnsRefId."'>".$vall->trnsRefId."</a></td>";
                                 echo "<td>N/A</td>"; 
                            echo "<td>".$vall->refuUserName."</td>"; 
                            echo "<td>".$vall->refuUserEmail."</td>"; 
                            echo "<td>".$vall->refuPaymentTypeName."</td>";  
                            
                           
                                         
                            echo "<td>".number_format($vall->refuPaidAmount,2,",",".")."</td>";
                            
                            
                            
                            if($vall->refuStatus == 1){
                               // $vall->refuStatus = '<span class="label label-success">'.$vall->refuStatus.'Confirmed</span>';
                            }else{
                                //$vall->refuStatus = '<span class="label label-warning">'.$vall->refuStatus.'</span>';
                            }
                            
                            echo "<td>".$vall->refuStatus."</td>";
                            
                            
                            echo "</tr>";
                            
                        
                        }
                    
                    }

                ?>
                
                </tbody>
                <tfoot>
                
                <tr>
				<?php if($EVENT==150) echo "<th>ID</th>"; ?>
                    <th>Date & Time</th>
                    <th>Order ID</th>
                    
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Payment Type</th>
                    <th>Amount</th>
                    <th>Status</th>
                    
                </tr>
                </tfoot>
              </table>
			  
              
              <!-- INFO -->
              
              <?php
                $total = 0;
                if(isset($data_all->status->totalRecords)){
            
                    $total = $data_all->status->totalRecords;
                
                }
                
                $tmp = $total/10;
                $tmp = ceil($tmp);
                $tmp_ = 0;
                $i = $pageNumber - 2;
                if($i <= 0){
                    $i = 1; 
                }
                
                $max = $pageNumber * $pageSize;
                $min = $max - $pageSize;
                if($max > $total){
                    $max = $total;
                }
                if($min <= 0){
                    $min = 1;
                }
                
              ?>
              <div class="row">
                <div class="col-sm-5">
                   
                   <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing <?php echo  $min;?>  to <?php echo  $max;?> of <?php echo  $total;?> entries</div>

                </div>
              
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                        
                        <?php
                        
                            $next = $pageNumber+1;
                            $prev = $pageNumber-1;
                            
                            if($prev <=0){
                                echo "<li class='paginate_button previous disabled'><a>Previous</a></li>";
                            }else{
                                echo "<li class='paginate_button previous'><a href='?pageNumber=$prev$search'>Previous</a></li>";
                            }
                            
                            
  
                            if($i <> 1){
                                echo "<li class='paginate_button'><a href='?pageNumber=1$search'>1</a></li>";
                                echo "<li class='paginate_button'><a>..</a></li>";
                                
                            }
  
                            
                            
                            for($i; $i < $tmp; $i++){
                                if($tmp_ == 5){
                                    echo "<li class='paginate_button'><a>..</a></li>";
                                    break;
                                }
                                
   
                                if($pageNumber == $i){
                                    echo "<li class='paginate_button active'><a href='?pageNumber=$i$search'> $i</a></li>";
                                }else{
                                    echo "<li class='paginate_button'><a href='?pageNumber=$i$search'>$i</a></li>";
                                }
                                
                                $tmp_++;
                                
                            }
                            if($tmp == $pageNumber){
                                echo "<li class='paginate_button active'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }else{
                                echo "<li class='paginate_button'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }
                            
                            
                            if($next >  $tmp){
                                echo "<li class='paginate_button next disabled' ><a>Next</a></li>"; 
                            }else{
                                echo "<li class='paginate_button next' ><a href='?pageNumber=$next$search'>Next</a></li>";
                            }
                            
                            
                        ?>

                            
                        </ul>
                    </div>
                </div>
				
			
              </div>
            
            
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2019 <a href="#">SteelytoeXyz</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $tmp_ref; ?>../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $tmp_ref; ?>../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $tmp_ref; ?>../dist/js/demo.js"></script>


<!-- DataTables -->
<script src="<?php echo $tmp_ref; ?>../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $tmp_ref; ?>../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo $tmp_ref; ?>../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : true
    })
  })
  
  
function myFunction() {
  var x = document.getElementById("search");
  x.value = x.value.toUpperCase();
}
  
  
</script>

</body>
</html>
