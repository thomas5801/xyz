<?php 
include __DIR__.'/../xyz/menu.php';

	

    $invoice = "n/a";
    if(isset($request[3])){
		$tmp = explode("?",$request[3]);
		$request[3] = $tmp[0];
		if(isset($tmp[1])){
			$resend_msg =  $tmp[1];
		}
		
        $invoice = $request[3];
    }



    // GET DATA
    $ch = curl_init(); 
                    
                    
    $url_ = $titu."api/v1/timeline/".$invoice;
                    
              

    // set url
    curl_setopt($ch, CURLOPT_URL, $url_);

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                

    // $output contains the output string 
    $output = curl_exec($ch); 
    
    // tutup curl 
    curl_close($ch);      

    // menampilkan hasil curl
    $data_all = json_decode($output);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Steelytoe Xyz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>Xyz</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Steelytoe</b>Xyz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> <?php echo $_SESSION['user']['EventName']; ?> &nbsp; </span>
              <i class="fa fa-calendar"> </i>
              <span class="label label-success"><?php echo COUNT($_SESSION['user']['Events']); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo COUNT($_SESSION['user']['Events']); ?> events</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php
                    foreach($_SESSION['user']['Events'] AS $vall){
                        echo "<li><a href='".'../xyz/event/'.$vall->evnhId."'><h3>".$vall->evnhName."</i></h3></a></li>";
                    }
                  
                ?>
 
                </ul>
              </li>
              <li class="footer"><a href="#">Close</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out"> </i>
            </a>

              
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
    <?php
    $menu = str_replace("{{transaction}}","class='active'",$menu);
    echo $menu;
  
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Timeline 
        <small><?php echo $invoice; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Participants</a></li>
        <li class="active">Timeline</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            
            <?php 
            
            if(isset($data_all->data)){
                $regis_time = date_format(date_create('0000-00-00 00:00:00'),"d F Y");
                
                foreach($data_all->data AS $vall){
                    
                    

                    $regis_time = date_format(date_create($vall->trnsDateTime),"d F Y");
                    echo "<li class='time-label'>";
                        echo "<span class='bg-red'>";
                        echo date_format(date_create($vall->trnsDateTime),"d F Y");
                        echo "</span>";
                    echo "</li>";
                    
                    
                    
                    echo "<li>";

                    if($vall->trnsGroup == 'EMAIL'){
                        echo "<i class='fa fa-envelope bg-blue'></i>";
                        echo "<div class='timeline-item'>";
                        echo "<span class='time'><i class='fa fa-clock-o'></i> ".date_format(date_create($vall->trnsDateTime),"H:i")."</span>";
                        echo "<h3 class='timeline-header'><b>".$vall->trnsTite."</b></h3>";
                        echo "<div > <html>".base64_decode($vall->trnsDescription)."</html></div>";
                        echo "<div class='timeline-footer'>";
                        
                    }else{
                        echo "<i class='ion ion-ios-cart-outline bg-blue'></i>";
                        echo "<div class='timeline-item'>";
                        echo "<span class='time'><i class='fa fa-clock-o'></i> ".date_format(date_create($vall->trnsDateTime),"H:i")."</span>";
                        echo "<h3 class='timeline-header'><b>".$vall->trnsTite."</b></h3>";
                        echo "<div class='timeline-body'> ".$vall->trnsDescription."</div>";
                        echo "<div class='timeline-footer'>"; 
                    }

                   // echo "</div>";
                    echo "</div>";
                    echo "</li>";
                   
                   /*
                   
                    if(isset($data_all->linked->lgntTrnsId )){
                       
                        foreach($data_all->linked->lgntTrnsId AS $vall){
                           
                           if($vall->lgntType == 1){
                               
                               echo "<li>";
                                echo "<i class='fa fa-envelope bg-blue'></i>";
                                echo "<div class='timeline-item'>";
                                    echo "<span class='time'><i class='fa fa-clock-o'></i> ".date_format(date_create($vall->trnsCreatedTime),"H:i")."</span>";
                                    echo "<h3 class='timeline-header'><a href='#'>Support Team</a> sent you an email</h3>";
                                    echo "<div class='timeline-body'> aaa</div>";
                                    echo "<div class='timeline-footer'>";
     
                                echo "</div>";
                                echo "</div>";
                                echo "</li>";
                           }
                        }                
                    }
                    
                    */
                }
                
            }
            
            
            
            ?>
            

            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
            
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2019 <a href="#">SteelytoeXyz</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
