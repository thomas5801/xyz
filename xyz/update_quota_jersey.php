<?php
include 'config.php';

$url = $titu."/api/v1/update_quota_jersey/".$_GET['quota_id'].'/'.$_GET['quota_total'];

    // ADD
    $header[] = 'Content-Type: application/json';
	$header[] = "Accept-Encoding: gzip, deflate";
	$header[] = "Cache-Control: max-age=0";
	$header[] = "Connection: keep-alive";
	$header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
   
   // curl_setopt($ch, CURLOPT_NOBODY, true);
   // curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']); 
   
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_ENCODING, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
    // tutup curl 
    curl_close($ch);      

    // menampilkan hasil curl
    
    $data_all = json_decode($output);

    if(!isset($data_all->status->error->message)){
        $data_all->message = "";
		header("Location: /../xyz/quota_jersey?id=".$_GET['quota_id']);
		exit;
    }else{
        $data_all->message = '&msg_bib='.$data_all->status->error->message;
		header("Location: /../xyz/crud_jersey/".$_GET['quota_id']."?id=".$data_all->message);
		exit;
    }
?>
