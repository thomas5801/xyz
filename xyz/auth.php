<?php
include 'config.php';

    // ADD
    if(isset($_POST['username'])  AND isset($_POST['password'])){
        
        $user = $_POST['username'];
        $pass = $_POST['password'];
      
        $hashed_string['username'] = $user;
        $hashed_string['password'] = $pass;
        
        $data_post = array(
            'data' => $hashed_string,
        );
        

   
      //  $response = get_content_login($url.'session/login', json_encode($data_post));
        $response = get_content_login($titu.'/api/v1/login', json_encode($data_post));
        
        unset($_POST['username']);
        unset($_POST['password']);
        
        $response = json_decode($response);
        

        
        if(isset($response->status->error->message)){
            header('Location: ' ."login.php?error=".$response->status->error->message);
            exit;
        }else{
            $_SESSION['user']['UserName']   = $response->data->userName;
            $_SESSION['user']['FullName']   = $response->data->userFullName;
            
            $_SESSION['user']['Events']     = $response->data->userEvent;
            if(isset($response->data->userEvent[0])){

                $_SESSION['user']['Event']          = $response->data->userEvent[0]->evnhId;
                $_SESSION['user']['EventName']      = $response->data->userEvent[0]->evnhName;
                $_SESSION['user']['Role']       =  $response->data->userEvent[0]->evnhRole;
            }else{
                $_SESSION['user']['Event']      = 0;
                $_SESSION['user']['Role']       = "Agent Rahasia";
                $_SESSION['user']['EventName']      = "N/A";
            }
            

            
            header("Location: dashboard");
            exit;
        }
		
    }

?>
