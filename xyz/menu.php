<?php



$_username  = $_SESSION['user']['UserName'];
$_fullname  = $_SESSION['user']['FullName'];
$_role      = $_SESSION['user']['Role'];


$response = get_content_login($titu.'api/v1/event/'.$_SESSION['user']['Event'], NULL);

$response = json_decode($response);





if($response->data->evnhType == 71){
    
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
		<li {{profile}}><a href='/xyz/profile'><i class='fa fa-calendar-minus-o'></i> <span>Profile</span><span class='pull-right-container'></span></a></li>
			  
		<li {{invite}}><a href='/xyz/invite'><i class='fa fa-hourglass-half'></i> <span>Invitation</span><span class='pull-right-container'></span></a></li>
        
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        <!--    
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>

        <li {{import}}><a href='/xyz/import'><i class='fa fa-user-plus'></i> <span>Import</span><span class='pull-right-container'></span></a></li>


        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}else if($response->data->evnhType == 2){
    
    $menu = "
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    
    <aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-tachometer-alt'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        <li {{profile}}><a href='/xyz/profile'><i class='fa fa-calendar-minus-o'></i> <span>Profile</span><span class='pull-right-container'></span></a></li>
		<li {{courier}}><a href='/xyz/courier'><i class='fa fa-truck'></i> <span>Courier</span><span class='pull-right-container'></span></a></li>
        
        <li {{runner}}><a href='/xyz/runner'><i class='fa fa-running'></i> <span>Runner</span><span class='pull-right-container'></span></a></li>
        <li {{runnerreport}}><a href='/xyz/runnerreport'><i class='fa fa-bar-chart'></i> <span>Runner Report</span><span class='pull-right-container'></span></a></li>
        
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        <!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket-alt'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>
        
        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
			
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}else if($response->data->evnhType == 99){
    
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        <li {{profile}}><a href='/xyz/profile'><i class='fa fa-calendar-minus-o'></i> <span>Profile</span><span class='pull-right-container'></span></a></li>
        <li {{report}}><a href='/xyz/report'><i class='fa fa-truck'></i> <span>Swab Test Results</span><span class='pull-right-container'></span></a></li>
       
  
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        
		
		
		
        <!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>

        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}
else{
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
        <li {{profile}}><a href='/xyz/profile'><i class='fa fa-calendar-minus-o'></i> <span>Profile</span><span class='pull-right-container'></span></a></li>
  <li {{invite}}><a href='/xyz/invite'><i class='fa fa-hourglass-half'></i> <span>Invitation</span><span class='pull-right-container'></span></a></li>
        
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{refundtransaction}}><a href='/xyz/refundtransaction'><i class='fa fa-refresh'></i> <span>Refund</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        
<li {{transfer_bib}}><a href='/xyz/transfer_bib'><i class='fa fa-repeat'></i> <span>Tansfer Bib</span><span class='pull-right-container'></span></a></li>
		<!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->

		<li class='treeview {{menu_ticket}}'>
          <a href='#'>
            <i class='fa fa-ticket'></i> <span>Tiket</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
				<li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-circle-o'></i> <span>RPC</span><span class='pull-right-container'></span></a></li>
				<li {{ticketV2}}><a href='/xyz/ticketV2'><i class='fa fa-circle-o'></i> <span>RPC V2</span><span class='pull-right-container'></span></a></li>
				
				<li {{delegation}}><a href='/xyz/delegation'><i class='ion ion-ios-people-outline'></i> <span>Delegation</span><span class='pull-right-container'></span></a></li>
			
		  </ul>
        </li>
		
		
		<li class='treeview {{menu_quota}}'>
          <a href='#'>
            <i class='fa fa-lock'></i> <span>Quota</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu' style ='{{menu_quota_show}}'>
				<li class ='{{quota_payment}}'><a href='/xyz/quota_payment'><i class='fa fa-circle-o'></i> <span>Quota Payment Type</span><span class='pull-right-container'></span></a></li>
				<li class ='{{quota_jersey}}'><a href='/xyz/quota_jersey'><i class='ion ion-ios-people-outline'></i> <span>Quota Jersey</span><span class='pull-right-container'></span></a></li>
        <li class ='{{quota_finisher}}'><a href='/xyz/quota_finisher'><i class='ion ion-ios-people-outline'></i> <span>Quota Finisher</span><span class='pull-right-container'></span></a></li>
        <li class ='{{quota_fast_track}}'><a href='/xyz/quota_fast_track'><i class='ion ion-ios-people-outline'></i> <span>Quota Fast Track</span><span class='pull-right-container'></span></a></li>
		
		  </ul>
        </li>
		
        <li {{import}}><a href='/xyz/import'><i class='fa fa-user-plus'></i> <span>Import</span><span class='pull-right-container'></span></a></li>


		<li class='treeview {{menu_summary}}'>
          <a href='#'>
            <i class='fa fa-desktop'></i> <span>Summary</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu' style ='{{menu_summary_show}}'>
				<li class ='{{summary_rpc}}'><a href='/ui/summary_rpc'><i class='fa fa-circle-o'></i> <span>Summary RPC</span><span class='pull-right-container'></span></a></li>
				<li class ='{{summary_participant}}'><a href='/ui/summary_participant'><i class='fa fa-circle-o'></i> <span>Summary Participant</span><span class='pull-right-container'></span></a></li>
				<li class ='{{summary_regis}}'><a href='/ui/summary_regis'><i class='fa fa-circle-o'></i> <span>Summary Registration</span><span class='pull-right-container'></span></a></li>
				<li class ='{{summary_participant_deleted}}'><a href='/ui/summary_participant_deleted'><i class='fa fa-circle-o'></i> <span>Summary Deleted Participants</span><span class='pull-right-container'></span></a></li>
				
		  </ul>
        </li>
        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        </section>
        <!-- /.sidebar -->
    </aside>";
}


   if($_role == 'Observer Event'){
	  
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
		
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
 }








?>